import sys
sys.path.append("C:/Users/lintonb/Documents/RubrikAxiomCode/axiom")
import RubrikFunctions
import time


dbname = 'master'
instancename = 'MALRUBRIK2'
pipelinevalue = 1
Mounted_DBName = "Axiom_Mount_" + dbname + "_" + instancename

Rubrikid = RubrikFunctions.getttingDatabase( dbname, instancename)

RubrikLastestRecoveryPoint = RubrikFunctions.gettingLastestRecoveryPoint(Rubrikid)

RubrikLivemountStatus = RubrikFunctions.liveMount( Rubrikid , RubrikLastestRecoveryPoint , dbname , instancename , Mounted_DBName ,pipelinevalue )
print(RubrikLivemountStatus)

time.sleep(30)

RubrikFunctions.unMount(Mounted_DBName,pipelinevalue)