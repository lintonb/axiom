# Module to truncate databases. Tables to keep or truncate can be specified.
# The DBMeta class will walk through the table structures, truncate dependencies, drop constraints and add them back afterwards when needed.

# Todo:
# Table structures are stored in Sqlite. We can move this to a custom class.
# Fix the split tables method!
# NArrow down the exception when truncate fails

import pyodbc
import argparse
import sqlite3
import time

#Class to hold action data
class Action:
    def __init__(self,sql: str, id: int = 0, depth:int = 0):
        self.depth = depth
        self.id = id
        self.sql = sql
        self.startime = 0
        self.endtime = 0
        self.hasrun = False

    def __str__(self) -> str:
        return self.sql

    def runtime(self) -> float:
        return self.endtime - self.startime
        
# Class for table data, with helper functions.
class Table:
    def __init__(self,schema: str,tablename: str):
        self.schema = schema
        self.tablename = tablename

    def __str__(self) -> str:
        return f"{self.schema}.{self.tablename}"

    def __eq__(self,other):
        if not isinstance(other,Table):
            return NotImplemented
        return self.schema == other.schema and self.tablename == other.tablename

    # Some table names have a . in them, so this is probably not safe.
    @classmethod
    def from_string(cls,tablename: str):
         name = tablename.split(".")
         return cls(name[0],name[1])

# Class that does all the work.
class DBMeta:
    def __init__(self,db_connection: pyodbc.Connection):
        # Sql to get a list of all foreign key refrences in the db except self refrencing foreign keys. 
        self.__constraint_sql="""
        SELECT 
        OBJECT_SCHEMA_NAME(f.parent_object_id) AS TableNameSchema, -- this
        OBJECT_NAME(f.parent_object_id) AS TableName,
        COL_NAME(fc.parent_object_id,fc.parent_column_id) AS ColumnName,
        OBJECT_SCHEMA_NAME(f.referenced_object_id) AS ReferenceTableNameSchema,
        OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
        COL_NAME(fc.referenced_object_id,fc.referenced_column_id) AS ReferenceColumnName,
        f.name AS ForeignKey
        FROM
        sys.foreign_keys AS f
        INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id
        INNER JOIN sys.objects AS o ON o.OBJECT_ID = fc.referenced_object_id
        where f.parent_object_id <> f.referenced_object_id
        ORDER BY TableNameSchema,TableName
        """

        #SQL to return all tables. 
        self._all_table_sql="SELECT SCHEMA_NAME(t.schema_id), t.name as table_view from sys.tables t order by table_view"

        #SQL to create table to store DB Meta data
        self.__fk_storage_sql = """
        CREATE TABLE "foreignkeys" (
            "id"	INTEGER PRIMARY KEY AUTOINCREMENT,
            "schema"	TEXT NOT NULL,
            "tablename"	TEXT NOT NULL,
            "refschema"	TEXT NOT NULL,
            "reftable"	TEXT NOT NULL,
            "name" TEXT NOT NULL
        );
        """

        # Constraints often have multiple columns maps. Store them here.
        self.__fk_cols_sql = """
        CREATE TABLE "refcols" (
            "id"	INTEGER NOT NULL,
            "left"	TEXT NOT NULL,
            "right"	TEXT NOT NULL,
            FOREIGN KEY("id") REFERENCES "foreignkeys"("id")
        );
        """

        cursor = db_connection.cursor()
        cursor.execute(self.__constraint_sql)
        rows = cursor.fetchall()

        # Create a db in memory. This usually takes up less than 1MB since it's only storing meta data.
        self.db_info = sqlite3.connect(":memory:")
        self.db_info.execute(self.__fk_storage_sql)
        self.db_info.execute(self.__fk_cols_sql)

        # Store all tables that have foreign key constraints along with their definitions. 
        fk_sql = "insert into foreignkeys (schema,tablename,refschema,reftable,name) values (?,?,?,?,?)"
        col_sql = "insert into refcols (id,left,right) values (?,?,?)"
        int_cur = self.db_info.cursor()
        for row in rows:
            int_cur.execute("select id from foreignkeys where schema = ? and name = ?",(row[0],row[6]))
            fk_row = int_cur.fetchone()
            if fk_row:
                fk_id = fk_row[0]
                int_cur.execute(col_sql,(fk_id,row[2],row[5]))
                self.db_info.commit()
            else:
                f = (row[0],row[1],row[3],row[4],row[6])
                int_cur.execute(fk_sql,f)
                fk_id = int_cur.lastrowid
                int_cur.execute(col_sql,(fk_id,row[2],row[5]))
                self.db_info.commit()

        # Store all tables, even those without FK constraints. 
        self.all_tables = []
        cursor.execute(self._all_table_sql)
        rows = cursor.fetchall()
        for row in rows:
            self.all_tables.append(Table(row[0],row[1]))

        self.to_do = []
        self.done = []
        self.actions = []
        self.keep_tables = []
        self._action_id = 0


    # Create a todo list from a list of tables to keep
    def create_keep_todo(self,keep_tbls: list):
        self.to_do = []
        for table in self.all_tables:
            if table not in keep_tbls:
                self.to_do.append(table)
        self.keep_tables = keep_tbls

    # Create a todo list from a list of tables to truncate. 
    def create_trunc_todo(self,trunc_tables : list):
        self.to_do = []
        self.to_do.extend(trunc_tables)
        for table in self.all_tables:
            if table not in trunc_tables:
                self.keep_tables.append(table)

    # Returns all tables that refrences given table. 
    def table_deps(self, table: Table) -> list:
        """Return all tables that refrence given schema and table"""
        cursor = self.db_info.cursor()
        cursor.execute("select schema,tablename from foreignkeys where refschema = ? and reftable = ?",(table.schema,table.tablename))
        rows = cursor.fetchall()
        table_deps = []
        for row in rows:
            new_dep = Table(row[0],row[1])
            if new_dep not in table_deps: table_deps.append(new_dep)
        return table_deps

    # Simple method to return SQL to drop a constraint. 
    @staticmethod
    def _fk_drop_sql(table : Table, fk_name: str) -> str:
        return f"alter table [{table.schema}].[{table.tablename}] drop constraint [{fk_name}]"

    # Method to re-create a constraint. It takes multi column mappings into account.     
    def _fk_create_sql(self,table: Table, fk_name: str) -> str:
        sql = f"alter table [{table.schema}].[{table.tablename}] add constraint [{fk_name}] foreign key "
        cursor = self.db_info.cursor()
        lookup = cursor.execute("select id,refschema,reftable from foreignkeys where schema = ? and tablename =? and name = ?",(table.schema,table.tablename,fk_name)).fetchone()
        fk_id = lookup[0]
        refschema = lookup[1]
        reftable = lookup[2]
        cursor.execute("select * from refcols where id = ?",(fk_id,))
        crows = cursor.fetchall()
        left_sql = "("
        right_sql = "("
        count=0
        crows_len = len(crows)
        for crow in crows:
            count += 1
            left_sql += f"[{crow[1]}]"
            right_sql += f"[{crow[2]}]"
            if count != crows_len:
                left_sql += ", "
                right_sql += ", "
        left_sql += ")"
        right_sql += ")"
        sql += f"{left_sql} references [{refschema}].[{reftable}] {right_sql}"
        return sql

    # Return a list for a given SQL query. 
    def rows_from_sql(self, sql: str, sql_params: tuple = None) -> list:
        cursor = self.db_info.cursor()
        if sql_params:
            cursor.execute(sql,sql_params)
        else:
            cursor.execute(sql)
        return cursor.fetchall()

    # This method will generate all sql needed to truncate a specific table.
    # Drop constraints for child tables that refrences this table
    # Drop contraints for the table to be truncated.
    # Truncate table
    # Re-create all constriants that were dropped. This included child tables.
    def truncate_sql(self,table: Table):
        sqls = []
        dep_tables = self.table_deps(table)
        for dep_table in dep_tables:
            rows = self.rows_from_sql("select * from foreignkeys where schema = ? and tablename = ? and refschema = ? and reftable = ?",\
                (dep_table.schema,dep_table.tablename,table.schema,table.tablename))
            for row in rows:
                sqls.append(self._fk_drop_sql(dep_table,row[5]))
        cursor = self.db_info.cursor()
        rows = cursor.execute("select * from foreignkeys where schema = ? and tablename = ?",(table.schema,table.tablename)).fetchall()
        for row in rows:
            sqls.append(self._fk_drop_sql(table,row[5]))
        sqls.append(f"truncate table [{table.schema}].[{table.tablename}]")
        for row in rows:
            sqls.append(self._fk_create_sql(table,row[5]))
        for dep_table in dep_tables:
            rows = self.rows_from_sql("select * from foreignkeys where schema = ? and tablename = ? and refschema = ? and reftable = ?",\
                (dep_table.schema,dep_table.tablename,table.schema,table.tablename))
            for row in rows:
                sqls.append(self._fk_create_sql(dep_table,row[5]))
        return sqls

    # Build actions to truncate a list of tables. 
    def build_truncate_set(self):
        if len(self.to_do) == 0: return

        for table in self.to_do:
            self._walk(table,-1)

    # Walk down the dependency tree to truncate a given table e.g.
    # Table A
    #   |_Table B
    #       |_ Table C
    # In this case truncate Table C, then B, then A
    def _walk(self,table : Table, depth: int):
        depth += 1
        deptables = self.table_deps(table)
        if len(deptables) > 0:
            for deptable in deptables:
                self._walk(deptable,depth)
        if table not in self.done:
            trunc_sql = self.truncate_sql(table)
            self.actions.extend(self.create_actions(trunc_sql,r_depth=depth))
            self.done.append(table)
        depth -= 1

    # Builds a set of actions. Not much use at the moment, meant for future use where we can build actions sets.
    def create_actions(self, actions: list, r_depth: int = 0) -> list:
        action_list = []
        for action in actions:
            action_list.append(Action(action,id=self._action_id,depth=r_depth))
            self._action_id += 1
        return action_list

# After truncation has been done, shrink the database, then shrink the reserved space for the files. 
# We need to enable autocommit for this to work.
def shrink_database(db_connection : pyodbc.Connection):
    db_connection.autocommit = True
    cursor = db_connection.cursor()
    cursor.execute("DBCC SHRINKDATABASE(?)",db_connection.getinfo(pyodbc.SQL_DATABASE_NAME))
    cursor.execute("""
    SELECT
    file_id,
    [name],
    [type],
    [type_desc],
    size/128.0 FileSizeInMB,
    CAST(FILEPROPERTY(name, 'SpaceUsed') AS int)/128.0 AS SpaceUsedInMB,
    size/128.0 - CAST(FILEPROPERTY(name, 'SpaceUsed') AS int)/128.0 AS EmptySpaceInMB
    FROM sys.database_files;
    """)
    rows = cursor.fetchall()
    for row in rows:
        filename = row[1]
        real_size = row [5]
        resize = int(real_size*2)
        if real_size < 5: resize = 5
        print(f"{filename} real size is {real_size} resizing to {resize}")
        sql = "DBCC SHRINKFILE (?, ?)"
        cursor.execute(sql,(filename,resize))
    cursor.close()
    db_connection.autocommit = False
        
# Backup database to a given location.
def backup_database(db_connection,path):
    db_connection.autocommit = True
    cursor = db_connection.cursor()
    db_name = db_connection.getinfo(pyodbc.SQL_DATABASE_NAME)
    sql = f"BACKUP DATABASE [{db_name}] TO  DISK = N'{path}' WITH NOFORMAT, NAME = N'{db_name}', NOINIT, SKIP, NOREWIND, NOUNLOAD,  STATS = 10"
    cursor.execute(sql)
    # Weirdness to allow database backup to complete. Found on stackoverflow after having issues. 
    while cursor.nextset():
        pass
    cursor.close()
    db_connection.autocommit = False

# Helper method to split schema.tablename to their different parts. Depends on an unsafe method!! Need to make it more robust.
def split_tables(tables: list) -> list:
    split_tables = []
    for table in tables:
        split_tables.append(Table.from_string(table))
    return split_tables

# Return a database connection.
def get_db_connection(server: str,db: str,username: str,password: str) -> pyodbc.Connection:
    """Return a database connection"""
    return pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+db+';UID='+username+';PWD='+ password)

# If run as standalone code, parse command line arguments. 
def main():
    parser = argparse.ArgumentParser(description='Truncate tables for a given database')
    parser.add_argument("--server", help="SQL Server instance",required=True)
    parser.add_argument("--database", help="Database to truncate",required=True)
    parser.add_argument("--username", help="Username for SQL Server",required=True)
    parser.add_argument("--password", help="Password for SQL Server",required=True)
    parser.add_argument("--keeptables", help="List of tables to exclude from truncation",nargs="+")
    args = vars(parser.parse_args())
    db_server = args["server"]
    db_database = args["database"]
    db_username = args["username"]
    db_password = args["password"]
    keep_tables= []
    if args["keeptables"]:
        keep_tables = args["keeptables"]

    truncate_db(db_server,db_database,db_username,db_password,keep_tables)

# Main function to run trunate operation.
def truncate_db(db_server,db_database,db_username,db_password,keep_tables=[]):
    conn = get_db_connection(db_server,db_database,db_username,db_password)
    with conn:
        # Read meta data from db.
        db_meta = DBMeta(conn)

    keep_tbls = split_tables(keep_tables)

    # Create a todo list from the tables that were passed.
    db_meta.create_keep_todo(keep_tbls)
    # Build truncate actions. 
    db_meta.build_truncate_set()

    # Some debug info. 
    print(f"Total tables: {len(db_meta.all_tables)}")
    print(f"Keep tables: {len(db_meta.keep_tables)}")
    print(f"Truncate tables: {len(db_meta.to_do)}")
    print(f"Total actions {len(db_meta.actions)}")

    # Run all the truncate actions. 
    db_conn = get_db_connection(db_server,db_database,db_username,db_password)
    with db_conn:
        cursor = db_conn.cursor()
        total_start = time.time()
        for action in db_meta.actions:
            try:
                # Print some debug info, time all actions
                print(f"Running action {action.sql} ... ",end="",flush=True)
                action.startime = time.time()
                cursor.execute(action.sql)
                cursor.commit()
                action.endtime = time.time()
                action.hasrun = True
                print(f"{action.runtime()} seconds")
            # Catch truncte errors, try and recover with a delete. This needs cleanup, exception needs to be more sepcific. 
            except Exception as e:
                if "truncate table" in action.sql:
                    try:
                        print("Truncate error occured. Trying delete...")
                        newsql = action.sql.replace("truncate table","delete from")
                        print(newsql)
                        cursor.execute(newsql)
                        cursor.commit()
                    except Exception as e:
                        print(f"Unrecovarable error - {e}")    
                        break
                else:
                    print(f"Unrecovarable error - {e}")
                    break

        # Shrink database after actions have run.
        shrinkstime = time.time()
        print("Shrinking database ... ",end="",flush=True)
        shrink_database(conn)
        print(f"{time.time() - shrinkstime}")

        #Backup database
        backup_database(conn,f"t:\\automated\\{conn.getinfo(pyodbc.SQL_DATABASE_NAME)}")

        print("Total execution time: {0} seconds".format(time.time() - total_start))

if __name__ == "__main__":
    # Some testing data
    tables=["dbo.tb_CasinoModuleSetting", "dbo.tb_AdvancedSlot", "dbo.tb_CurrencyModuleSetting", "dbo.tb_DefaultModuleSetting", "dbo.tb_GameSettings", "dbo.tb_CasinoDBVersion", "dbo.tb_CasinoDBVersionChangeLog", "dbo.tb_ModuleSettingDefaultValue", "dbo.tb_DBInstall", "dbo.tb_BettingModel", "dbo.tb_GameConfig", "dbo.tb_ModuleSetting", "dbo.tb_CurrencyCasinoSettings", "dbo.tb_IPAddressMask", "dbo.tb_Module", "dbo.tb_Setting", "dbo.tb_CasinoDBVersionBatch", "dbo.tb_BasicGameGroupMember", "dbo.tb_Currency", "BankingClient.tb_BankingConfig", "dbo.tb_BankingConfigData", "BankingClient.tb_BankingConfigLookupType", "BankingClient.tb_BankingFeature", "dbo.tb_BankingPspResponseCode", "dbo.tb_BankingServiceApplication", "dbo.tb_BankProfileNonSwitchable", "dbo.tb_BankProfileProcessingCurrencies", "dbo.tb_BankProfileSupportedCurrencies", "dbo.tb_BetModel", "dbo.tb_ExternalInstalledClients", "dbo.tb_GameConfigControl", "dbo.tb_GameGroup", "dbo.tb_GameGroupType", "dbo.tb_GameModulePackageName", "dbo.tb_GameProfile", "dbo.tb_GameTableSequence", "dbo.tb_GamingService", "dbo.tb_HistoricalCountryList", "dbo.tb_LinkedServers", "dbo.tb_LinkedServerSources", "dbo.tb_LoyaltySetting", "dbo.tb_MachineAttribute", "dbo.tb_OperatorCasino", "dbo.tb_Product", "dbo.tb_RegulatedMarketModuleGameTypeMap", "dbo.tb_RegulatedMarketPaymentMethodType", "dbo.tb_RegulatedMarketPurchaseMethodType", "dbo.tb_RegulatedMarketType", "nsf.tb_RemoteServerSources", "dbo.tb_ServerIDAlternate", "dbo.tb_ServiceControlInfo", "api.tb_Setting", "dbro.tb_Setting", "dbo.tb_SettingCategory", "dbo.tb_SettingClass", "dbo.tb_SettingLegacyName", "dbo.tb_SettingTemplate", "dbo.tb_SystemSetting", "dbo.tb_TermsAndConditionsVersion", "dbo.tb_Territory", "dbo.tb_ApplicationModule", "dbo.tb_ApplicationSystem", "dbo.tb_ApplicationSystemTable", "dbo.tb_BasicGameGroup", "dbo.tb_BetModelSetting", "dbo.tb_BonusEventType", "dbo.tb_BonusMode", "dbo.tb_CasinoSetting", "dbo.tb_CasinoSupportedBasketType", "dbo.tb_CasinoSupportedCashinMethod", "dbo.tb_CasinoSupportedLanguage", "dbo.tb_CurrencyCountryCasinoSettings", "dbo.tb_CurrencyGameProfile", "dbo.tb_CurrencySetting", "dbo.tb_DefaultSetting", "dbo.tb_ExternalProduct", "dbo.tb_ExternalProductSetting", "dbo.tb_ExternalSetting", "dbo.tb_GameEvent", "dbo.tb_GamingGroupSetting", "dbo.tb_GamingGroupSettingAuditLog", "dbo.tb_InstalledCasino", "dbo.tb_InstalledCasinoClientType", "dbo.tb_InstalledGames", "dbo.tb_Operator", "Account.tb_OperatorAttributeDefault", "dbo.tb_OperatorBaseCurrency", "dbo.tb_OperatorEvents", "dbo.tb_OperatorGameAttribute", "dbo.tb_OperatorLogin", "dbo.tb_PlayerProtectionRegulatedMarketValidSetting", "dbo.tb_RegQData", "dbo.tb_RegQOptional", "dbo.tb_RegQuestions", "dbo.tb_RegulatedConfig", "dbo.tb_RegulatedMarket_ProvinceToRegulatoryAuthorityIdentifierMap", "dbo.tb_RegulatedMarket_RegulatoryAuthorityDescription", "dbo.tb_RegulatedMarket_RegulatoryAuthorityProvinceDescription", "dbo.tb_RegulatedMarket_UserAttributeToRegulatoryAuthorityIdentifierMap", "dbo.tb_RegulatedMarketAdhocCurrencySettings", "dbo.tb_RegulatedMarketAdhocSettings", "dbo.tb_RegulatedMarketConfigSetting", "Account.tb_RegulatedMarketDataLogType", "dbo.tb_RegulatedMarketGameType", "dbo.tb_RegulatedMarketJurisdiction", "dbo.tb_RegulatedMarketSystem", "dbo.tb_RegulatedSetting", "dbo.tb_RegulatedStatusCodeMap", "dbo.tb_RegulatedMarketBinMap", "dbo.tb_RegulatedMarketCountryMap", "Account.tb_RegulatedMarketDataLog", "dbo.tb_RegulatedMarketOLRCountryCurrency", "dbo.tb_RegulatedMarketOLRCountryList", "dbo.tb_RegulatedMarketOLRCurrencyList"]
    truncate_db("MAL2397\\MALRUBRIK2","Axiom_Mount_Casino","sa","QuC4YF1re",tables)