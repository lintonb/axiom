import argparse
import requests
import json
import datetime
import time
import urllib3
from calendar import timegm
r = requests.session()
urllib3.disable_warnings()  
  
r.headers.update({'Content-Type':'application/json' })

#Setting up Arguments to begin script for build

parser = argparse.ArgumentParser()
parser.add_argument("-db", "--Database",help="Provide database to pass through Pipeline , eg:(MALQFCAS1)",required=True)
parser.add_argument("-Int", "--Instance",help="Provide related Instance associated , eg:(MALQFCAS1)",required=True)
args = parser.parse_args()
#define Datbase from input values

DatabaseName = args.Database
InstanceName = args.Instance

def main():
    print("Starting to fetch data from Rubrik to retrieve selected database")
    RubrikDatabase = getttingDatabase(DatabaseName,InstanceName)
    if RubrikDatabase == None:
        print("Cannot Find Database name " + DatabaseName + " for instance " + InstanceName)
        exit()
    else:
        print("Found database " + str(DatabaseName) + ", Rubrik db id is "+ str(RubrikDatabase))
    print("Fetching the last recovery Point for the database")     
    LatestRecoveryPoint = gettingLastestRecoveryPoint(RubrikDatabase)
    if LatestRecoveryPoint == None:
        print("Cant find lastest rcovery point for " + DatabaseName)
        exit()
    else:
        print("The lastest snapshot for the database is " + str(LatestRecoveryPoint))
    print("Starting to attempt the live mount")
    AxiomLiveMount = Livemount(RubrikDatabase ,LatestRecoveryPoint,DatabaseName)
    print ("Starting to monitor request " + AxiomLiveMount)
    LiveMountStatus = FetchJobStatus(AxiomLiveMount)
    print("Live mount for " + Mounted_DBName + " " + LiveMountStatus)
    if LiveMountStatus == "FAILED":
        print("Failed to live mount " + Mounted_DBName)
        exit()
    else:
        print("Live mount completed successfully , moving on...")
    
    

#Function to gather information about database

def getttingDatabase (RubrikDatabaseName,RubrikInstance):
    mainlist = r.get('https://10.72.5.130/api/v1/mssql/db', auth=() , verify=False)
    if mainlist.status_code != 200:
        raise ValueError ("error Connecting to Rubrik to retrieve full databaselist")
        exit()
    query_object = json.loads(mainlist.text)
    for db in query_object['data']:
        if db['name'] == RubrikDatabaseName and db['instanceName'] == RubrikInstance:
            database = db['id']
            return (database)


def gettingLastestRecoveryPoint(RubrikDatabaseid):
    x = 0
    RubrikDB = r.get('https://10.72.5.130/api/v1/mssql/db/'+RubrikDatabaseid+'/snapshot?primary_cluster_id=local' + RubrikDatabaseid, auth=() , verify=False)
    query_object = json.loads(RubrikDB.text)
    max_objects = query_object['total']
    for snapshot in query_object['data']:
        x += 1
        if x == max_objects:
            snapshot_date = snapshot['date']
            utc_time = time.strptime(snapshot_date, "%Y-%m-%dT%H:%M:%S.%fZ")
            epoch_time = timegm(utc_time)
            timestamp = str(epoch_time) + "000"
            return (timestamp)


def Livemount(RubrikDatabaseid ,RubrikSnapshotTime,RubrikDatabaseName):
    global Mounted_DBName
    Mounted_DBName = "Axiom_Mount_" + DatabaseName
    payload = {'recoveryPoint': {'timestampMs': int(RubrikSnapshotTime)},'mountedDatabaseName': Mounted_DBName,'targetInstanceId':'MssqlInstance:::883d6e7e-a648-447d-a079-0852fc6e7b57'}
    LiveMount = r.post('https://10.72.5.130/api/v1/mssql/db/' + RubrikDatabaseid + '/mount', auth=() , data=json.dumps(payload) , verify=False)
    if LiveMount.status_code != 202:
        raise ValueError ("Something Went Wrong with Live mount")
        exit()
    reuqestid = str(json.loads(LiveMount.text)["id"])
    return (reuqestid)


def FetchJobStatus(RubrikRequestid):
    requestid =r.get('https://10.72.5.130/api/v1/mssql/request/'+ RubrikRequestid , auth=() , verify=False)
    requeststatus = str(json.loads(livemountid.text)["status"])
    while requeststatus not in ['SUCCEEDED', 'FAILED']:
        time.sleep(2)
        requestid = r.get('https://10.72.5.130/api/v1/mssql/request/'+ RubrikRequestid , auth=() , verify=False)
        requeststatus = str(json.loads(livemountid.text)["status"])
        print("The Status of the job is " + livemountstatus)
    return(livemountstatus)

def fileset ():
    

#def sql(RubrikDatabaseName):

# Start program
if __name__ == "__main__":
   main()

