import argparse
import requests
import json
import datetime
import time
import urllib3
import sys
from calendar import timegm
from datetime import datetime
r = requests.session()
urllib3.disable_warnings()  
  
r.headers.update({'Content-Type':'application/json' })

username = ""
password = ""

def getttingDatabase (RubrikDatabaseName,RubrikInstance):
    print("Fetching main Rubrik list")
    mainlist = r.get('https://10.40.5.130/api/v1/mssql/db', auth=(username , password) , verify=False)
    if mainlist.status_code != 200:
        raise ValueError ("error Connecting to Rubrik to retrieve full databaselist")
        exit()
    query_object = json.loads(mainlist.text)
    database = ""
    print("Gathered Rubrik list , fetching our selected database " + RubrikDatabaseName)
    for db in query_object['data']:
        if db['name'] == RubrikDatabaseName and db['instanceName'] == RubrikInstance:
            database = db['id']
            print("Found our database " + RubrikDatabaseName)
            return (database)
    if database == "":
        raise ValueError ("Error trying to find the selected database from Rubrik")
        exit()
    


def gettingLastestRecoveryPoint(RubrikDatabaseid):
    RubrikDB = r.get('https://10.40.5.130/api/v1/mssql/db/'+RubrikDatabaseid+'/snapshot?primary_cluster_id=local' + RubrikDatabaseid, auth=(username , password) , verify=False)
    if RubrikDB.status_code != 200:
        raise ValueError ("Error while connecting to Rubrik to retrieve object recovery points")
        exit()
    query_object = json.loads(RubrikDB.text)
    max_objects = query_object['total']
    if max_objects == "":
        raise ValueError ("Error , object has no recovery points")
        exit()
    timestamp = ""
    print("Finding the lastest recovery point for the database")
    rubrikdata = query_object['data']
    sorted_date = sorted(rubrikdata, key=lambda x: datetime.strptime(x['date'], "%Y-%m-%dT%H:%M:%S.%fZ") , reverse=True )
    lastrecovery = sorted_date[0]['date']
    print("Lastest rcovery point is " + lastrecovery)
    utc_time = time.strptime(lastrecovery, "%Y-%m-%dT%H:%M:%S.%fZ")
    epoch_time = timegm(utc_time)
    timestamp = str(epoch_time) + "000"
    if timestamp == "":
        raise ValueError ("Error converting timestamp")
        exit()

    return (timestamp)
   

def liveMount(RubrikDatabaseid ,RubrikSnapshotTime,RubrikDatabaseName,RubrikInstance,Mounted_DBName,PipelineBuild):
    payload = {'recoveryPoint': {'timestampMs': int(RubrikSnapshotTime)},'mountedDatabaseName': Mounted_DBName,'targetInstanceId':'MssqlInstance:::9db27467-2592-4e28-8649-5f191860fa6f'}
    LiveMount = r.post('https://10.40.5.130/api/v1/mssql/db/' + RubrikDatabaseid + '/mount', auth=(username , password) , data=json.dumps(payload) , verify=False)
    if LiveMount.status_code != 202:
        raise ValueError ("Something Went Wrong with Live mount when sending payload to Rubrik")
        exit()
    requestid = str(json.loads(LiveMount.text)["id"])
    Livemountstatus = fetchJobStatus(requestid,Mounted_DBName,PipelineBuild)
    return(Livemountstatus)



def fetchJobStatus(RubrikRequestid,Jobdetails,PipelineBuild):
    requestid =r.get('https://10.40.5.130/api/v1/mssql/request/'+ RubrikRequestid , auth=(username , password) , verify=False)
    requeststatus = str(json.loads(requestid.text)["status"])
    while requeststatus not in ['SUCCEEDED', 'FAILED']:
        time.sleep(2)
        requestid = r.get('https://10.40.5.130/api/v1/mssql/request/'+ RubrikRequestid , auth=(username , password) , verify=False)
        requeststatus = str(json.loads(requestid.text)["status"])
        print("The Status of the job is " + requeststatus)
    finished = {
        "Job_details": Jobdetails , 
        "RequestStatus":requeststatus ,
        "Pipeline": PipelineBuild
        }
    return(finished)


def unMount(Mounted_DBName,PipelineBuild):
     requestdata =r.get('https://10.40.5.130/api/v1/mssql/db/mount' , auth=(username , password) , verify=False)
     if requestdata.status_code != 200:
        raise ValueError ("Error while trying to fetch mount points to unmount")
        exit()
     query_object = json.loads(requestdata.text)
     mounts = query_object['data']
     mountedDatabaseid = ""
     for mount in mounts:
         if mount['mountedDatabaseName'] == Mounted_DBName:
             print("Attempting to unmount database " + Mounted_DBName)
             mountedDatabaseid = mount['id']
             requestunmount =r.delete('https://10.40.5.130/api/v1/mssql/db/mount/' + mountedDatabaseid, auth=(username , password) , verify=False)
             if requestunmount.status_code != 202:
                raise ValueError ("Error while trying to unmount database" + Mounted_DBName)
                exit() 
             print("Sucessfully Unmounted Database " + Mounted_DBName)
             



    






